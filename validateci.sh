#!/bin/bash
code=0
RD='\033[1;31m'
GR='\033[1;32m'
YL='\033[1;33m'
NC='\033[0m'
while IFS='' read -r -d '' filename; do  
    echo -e "${YL}validating\t\t$filename${NC}"
    jsondata=`jq -n '{ content: '"$(jq -Rs . <<< $(cat $filename ))"' }'`
    result=$(curl -s --header 'Content-Type: application/json' 'https://gitlab.com/api/v4/ci/lint' --data "$jsondata")
    status=$(echo $result | jq -r '.status')

    if [ "$status" == "valid" ]; then
        echo -e "${GR}passed\t\t$filename${NC}"
    else
        echo -e "${RD}failed\t\t$filename${NC}"
        echo -e "${RD}$(echo $result | jq '.errors|to_entries[]' |jq -r '"\t\(.key+1):\t\(.value)"')${NC}"
        code=1
    fi
  if [ $? -eq 1 ]; then exit 1; fi
done < <(find . -type f -name "*.yml" -print0)
exit $code